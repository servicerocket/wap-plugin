/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import java.io.UnsupportedEncodingException;
import java.util.Collections;
import java.util.List;

import org.randombits.confluence.support.sorting.NaturalTitleComparator;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Page;
import com.atlassian.core.util.FileSize;

/**
 * TODO: Document this class.
 *
 * @author David Peterson
 */
public class ViewPageAction extends AbstractPageAction
{
    private ContentPropertyManager contentPropertyManager;
    private NotificationManager notificationManager;

    private List children;
    private String excerpt;

    public String getPageExcerpt()
    {
        if (excerpt == null)
        {
            excerpt = contentPropertyManager.getTextProperty(getPage(), ExcerptMacro.EXCERPT_KEY);
            if (excerpt == null)
                excerpt = getPage().getExcerpt();
        }
        return formatExternalLinks(getGeneralUtil().escapeXMLCharacters( excerpt ));
    }

    private String formatExternalLinks(String excerpt) {
        String linked = "";
        String [] tokens = excerpt.split( " " );
        for(int i=0;i<tokens.length;i++) {
            if(containsLinkFormat(tokens[i])) {
                String newToken = "<a href=\"" + tokens[i] + "\">" + tokens[i].replace( "mailto:", "" ) + "</a>";
                linked += " " + newToken;
            }
            else {
                linked += " " + tokens[i];
            }
        }
        return linked;
    }
    
    private boolean containsLinkFormat(String word) {
        if(word.toLowerCase().contains( "http:" ) || word.toLowerCase().contains( "https:" ) || word.toLowerCase().contains( "mailto:" )) {
            return true;
        }
        return false;
    }
    
    public int getAttachmentCount()
    {
        List attachments = getAttachments();
        if (attachments == null)
            return 0;
        return attachments.size();
    }

    public List getChildren()
    {
        if (children == null)
        {
            AbstractPage apage = getPage();
            if (apage instanceof Page)
            {
                children = removeInvisibleContent(((Page) apage).getChildren());
                Collections.sort(children, new NaturalTitleComparator());
            }
        }
        return children;
    }

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public String getNiceSourceSize()
    {
        if (getPage() != null && getPage().getContent() != null)
        {
            String enc = getGlobalSettings().getDefaultEncoding();
            if (enc == null)
                enc = "UTF-8";
            try
            {
                int size = getPage().getContent().getBytes(enc).length;
                return FileSize.format(size);
            }
            catch (UnsupportedEncodingException e)
            {
                return FileSize.format(getPage().getContent().length());
            }
        }
        return FileSize.format(0);
    }

    public boolean isWatchingSpace()
    {
        if (getRemoteUser() != null)
        {
            return notificationManager.getNotificationByUserAndSpace(getRemoteUser(), getSpaceKey()) != null;
        }
        return false;
    }

    public boolean isWatchingPage()
    {
        if (getRemoteUser() != null)
        {
            return notificationManager.getNotificationByUserAndPage(getRemoteUser(), getPage()) != null;
        }
        return false;
    }

    public NotificationManager getNotificationManager()
    {
        return notificationManager;
    }

    public void setNotificationManager(NotificationManager notificationManager)
    {
        this.notificationManager = notificationManager;
    }
}
