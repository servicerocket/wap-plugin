/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.actions.SpaceAware;
import org.randombits.confluence.support.sorting.NaturalTitleComparator;

import java.util.Collections;
import java.util.List;

/**
 * Views information about the space.
 *
 * @author David Peterson
 */
public class ViewSpaceAction extends WapActionSupport implements SpaceAware
{
    private Space space;
    private PageManager pageManager;
    private List topLevelPages;

    public List getTopLevelPages()
    {
        if (topLevelPages == null)
        {
            topLevelPages = removeInvisibleContent(pageManager.getTopLevelPages(getSpace()));
            Collections.sort(topLevelPages, new NaturalTitleComparator());
        }
        return topLevelPages;
    }

    public void setSpace(Space space)
    {
        this.space = space;
    }

    public boolean isSpaceRequired()
    {
        return true;
    }

    public boolean isViewPermissionRequired()
    {
        return true;
    }

    public Space getSpace()
    {
        return space;
    }

    public void setPageManager(PageManager pageManager)
    {
        this.pageManager = pageManager;
    }
}
