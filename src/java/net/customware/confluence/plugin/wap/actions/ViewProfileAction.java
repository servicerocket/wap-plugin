/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import com.atlassian.confluence.user.UserPreferencesKeys;
import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.user.User;
import com.opensymphony.module.propertyset.PropertyException;
import com.opensymphony.module.propertyset.PropertySet;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * TODO: Document this class.
 *
 * @author David Peterson
 */
public class ViewProfileAction extends WapActionSupport
{
    private String username;
    private User user;

    public String getUsername()
    {
        if ((username == null || StringUtils.isEmpty(username)) && getRemoteUser() != null)
            username = getRemoteUser().getName();
        return username;
    }

    public void setUsername(String username)
    {
        if (GeneralUtil.shouldUrlDecode(username))
            username = GeneralUtil.urlDecode(username);

        this.username = username;
    }

    public User getUser()
    {
        if ((user == null) && StringUtils.isNotEmpty(getUsername()))
            user = userAccessor.getUser(getUsername());

        return user;
    }

    public boolean isViewingMyProfile()
    {
        return (getUser() != null && getUser().equals(getRemoteUser()));
    }

    public boolean isEmailVisible()
    {
        return isViewingMyProfile() || super.isEmailVisible();
    }

    public String renderEmail(String email) {
        return GeneralUtil.maskEmail(email);
    }

    public Date getSignupDate()
    {
        try
        {
            return getDateProperty(UserPreferencesKeys.PROPERTY_USER_SIGNUP_DATE);
        }
        catch (Exception e)
        {
            log.error("An error occurred trying to retrieve signup date");
            e.printStackTrace();
        }

        return null;
    }

    public Date getLastLoginDate()
    {
        try
        {
            return getDateProperty(UserPreferencesKeys.PROPERTY_USER_PREVIOUS_LOGIN_DATE);
        }
        catch (Exception e)
        {
            log.error("An error occurred trying to retrieve last login date");
            e.printStackTrace();
        }

        return null;
    }

    /**
     * A backward compatibility method
     *
     * 'signup' and 'last login' date properties are now actual Date objects but were stored as longs before.
     * This method checks the type of the property before fetching the value with the appropriate getter method
     *
     * @param propertyKey The property key.
     * @return The date stored in the property.
     */
    private Date getDateProperty(String propertyKey)
    {
        PropertySet propertySet = userAccessor.getPropertySet(getUser());
        if (propertySet.exists(propertyKey))
        {
            try
            {
                long aLong = propertySet.getLong(propertyKey);
                if (aLong > 0)
                    return new Date(aLong);
                else // really ugly, but no way around it. getPropertySet().getType(key) is broken, so we can't check for types
                    return propertySet.getDate(propertyKey);
            }
            catch (PropertyException e)
            {
                return propertySet.getDate(propertyKey);
            }
        }
        else
            return null;
    }

}
