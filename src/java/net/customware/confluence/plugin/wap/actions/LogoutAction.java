/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import com.opensymphony.webwork.ServletActionContext;
import com.atlassian.seraph.auth.DefaultAuthenticator;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.confluence.event.events.security.LogoutEvent;

import javax.servlet.http.HttpSession;
import java.security.Principal;

/**
 * Log out of Confluence.
 *
 * @author David Peterson
 */
public class LogoutAction extends com.atlassian.confluence.user.actions.LogoutAction
{
    public String execute() throws Exception
    {
        HttpSession session = ServletActionContext.getRequest().getSession();

        Principal user = (Principal) session.getAttribute(DefaultAuthenticator.LOGGED_IN_KEY);
        session.removeAttribute("confluence_security_originalurl");
        SecurityConfigFactory.getInstance().getAuthenticator().logout(ServletActionContext.getRequest(),
            ServletActionContext.getResponse());

        Boolean loggedOut = (Boolean) session.getAttribute(DefaultAuthenticator.LOGGED_OUT_KEY);

        if (loggedOut.booleanValue())
        {
            String remoteHost = ServletActionContext.getRequest().getRemoteHost();
            String remoteIP = ServletActionContext.getRequest().getRemoteAddr();

            if (user != null)
                eventManager.publishEvent(new LogoutEvent(this, user.getName(), session.getId(), remoteHost, remoteIP));
        }

        return super.execute();
    }

    public boolean isPermitted()
    {
        return true;
    }
}
