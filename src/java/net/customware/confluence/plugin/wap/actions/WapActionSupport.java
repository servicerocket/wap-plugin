/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import java.util.List;
import java.util.ListIterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.randombits.confluence.support.PluginActionSupport;

import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.util.GeneralUtil;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

/**
 * Provides support to WAP Actions.
 *
 * @author David Peterson
 */
public abstract class WapActionSupport extends PluginActionSupport
{
    public static final String XHTML_MP_CONTENT_TYPE = "application/vnd.wap.xhtml+xml";
    public static final String XHTML_CONTENT_TYPE = "application/xhtml+xml";
    public static final String HTML_CONTENT_TYPE = "text/html";

    private GeneralUtil generalUtil = new GeneralUtil();
    private String referer;
    private Settings settings;

    public String execute() throws Exception
    {
        String status = doExecute();
        applyContentType();
        return status;
    }

    protected List removeInvisibleContent(List content)
    {
        if (content != null)
        {
            ListIterator i = content.listIterator();
            while (i.hasNext())
            {
                Object object = i.next();
                if (!permissionManager.hasPermission(getRemoteUser(), Permission.VIEW, object))
                    i.remove();
            }
        }

        return content;
    }

    protected String doExecute() throws Exception
    {
        return SUCCESS;
    }

    protected static void applyContentType()
    {
        getResponse().setContentType(getContentType());
    }

    /**
     * Returns the content type for this action.
     *
     * @return The content type.
     */
    public static String getContentType()
    {
        HttpServletRequest request = getRequest();
        String acceptHeader = request != null ? request.getHeader("accept") : null;

        if (acceptHeader != null)
        {
            if (acceptHeader.indexOf(XHTML_MP_CONTENT_TYPE) != -1)
                return XHTML_MP_CONTENT_TYPE;
            else if (acceptHeader.indexOf(XHTML_CONTENT_TYPE) != -1)
                return XHTML_CONTENT_TYPE;
        }

        return HTML_CONTENT_TYPE;
    }

    public static HttpServletRequest getRequest()
    {
        return ServletActionContext.getRequest();
    }

    public static HttpServletResponse getResponse()
    {
        return ServletActionContext.getResponse();
    }

    public GeneralUtil getGeneralUtil()
    {
        return generalUtil;
    }

    /**
     * @return null if no valid url found.
     */
    public String getRefererURL()
    {
        if (referer == null)
        {
            HttpServletRequest request = getRequest();
            referer = request.getHeader("Referer");

            if (!TextUtils.stringSet(referer))
                return null;
    
            // If the referrer is "logout", make sure we don't set that as the URL to return to!
            if (referer.indexOf("logout") > -1 || referer.indexOf("login") > -1 )
                return null;

            String baseUrl = getSettings().getBaseUrl();

            if (referer.indexOf("://") != -1 && referer.startsWith(baseUrl)) // try to drop off baseurl if referer URL is absolute
            {
                referer = referer.substring(baseUrl.length());
                if (!referer.startsWith("/"))
                    referer = "/" + referer;
            }
        }

        return referer;
    }

    private Settings getSettings()
    {
        return settings;
    }

    public void setSettings(Settings settings)
    {
        this.settings = settings;
    }
}
