package net.customware.confluence.plugin.wap.actions;

import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;

import java.util.Map;

/**
 * <p>Designate a certain part of a page to be the "excerpt". The body contents of the macro will now be
 * rendered to ensure consistency between page that has an excerpt and also other macros that include the
 * excerpt content. (CONF-6342)
 *
 * <p>Note that the actual content property stored with the page will be wiki markup. Other macros that include
 * the excerpt (such as the excerpt-include) will need to be render it with the correct context so that things
 * like links, images etc.. are rendered and displayed correctly.
 */
public class ExcerptMacro extends BaseMacro
{
    public static final String EXCERPT_KEY = "confluence.excerpt";
    private ContentPropertyManager contentPropertyManager;

    public void setContentPropertyManager(ContentPropertyManager contentPropertyManager)
    {
        this.contentPropertyManager = contentPropertyManager;
    }

    public boolean isInline()
    {
        return true;
    }

    public boolean hasBody()
    {
        return true;
    }

    public RenderMode getBodyRenderMode()
    {
        return null;
    }

    public String execute(Map parameters, String body, RenderContext renderContext) throws MacroException
    {
        if (renderContext instanceof PageContext)
        {
            PageContext pageContext = (PageContext) renderContext;
            
            // Ensure we're only recording excerpts of the base page
            if (pageContext.getOriginalContext() == pageContext && pageContext.getEntity() != null)
            {
                // store the wiki markup
                contentPropertyManager.setTextProperty(pageContext.getEntity(), EXCERPT_KEY, body);
            }
        }
        
        return shouldHideExcerpt(parameters) ? "" : body;
    }

    private boolean shouldHideExcerpt(Map parameters)
    {
        return "true".equalsIgnoreCase((String)parameters.get("hidden"));
    }
}
