/*
 * Copyright (c) 2007, CustomWare Asia Pacific
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "CustomWare Asia Pacific" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package net.customware.confluence.plugin.wap.actions;

import java.util.Date;

import org.apache.velocity.app.FieldMethodizer;

import com.atlassian.confluence.util.GeneralUtil;
import com.atlassian.seraph.config.SecurityConfigFactory;
import com.atlassian.seraph.filter.LoginFilter;
import com.opensymphony.util.TextUtils;
import com.opensymphony.webwork.ServletActionContext;

/**
 * TODO: Document this class.
 *
 * @author David Peterson
 */
public class LoginAction extends WapActionSupport
{
    private boolean fromNotPermitted = false;
    private String os_username;
    private String os_destination;

    /**
     * On successful login, redirection will be handled by the LoginFilter based on os_destination or the original URL stored in the session
     */
    public String doExecute() throws Exception
    {
        if (os_destination == null)
            os_destination = "/plugins/wap/dashboard.action";

        // remove any stale original URL's in the session?
        ServletActionContext.getContext().getSession().remove(SecurityConfigFactory.getInstance().getOriginalURLKey());

        // if user does not specify an os_destination in the login url declared in seraph-config.xml, we can use the referer as backup
        String refererURL = getRefererURL();
        if (!TextUtils.stringSet(os_destination) && TextUtils.stringSet(refererURL))
            ServletActionContext.getContext().getSession().put(SecurityConfigFactory.getInstance().getOriginalURLKey(), refererURL);

        return SUCCESS;
    }

    public boolean isPermitted()
    {
        return true;
    }

    public boolean isFromNotPermitted()
    {
        return fromNotPermitted;
    }

    public void setFromNotPermitted(boolean fromNotPermitted)
    {
        this.fromNotPermitted = fromNotPermitted;
    }

    public FieldMethodizer getLoginFilter()
    {
        return new FieldMethodizer(new LoginFilter());
    }

//    public WebRequestUtils getWebRequestUtils()
//    {
//        return new WebRequestUtils();
//    }

    public String getOs_username()
    {
        GeneralUtil.format( new Date() );
        return os_username;
    }

    public void setOs_username(String os_username)
    {
        this.os_username = os_username;
    }

    public String getOs_destination()
    {
        return os_destination;
    }

    public void setOs_destination(String os_destination)
    {
        this.os_destination = os_destination;
    }

}
